## Iterables can produce iterators.
'''
Iterables can be used to produce iterators, explicitly or implicitly. When you call iter() function on an iterable, you get an iterator object. The object reference can be saved to a variable. To step through the iterator explicitly, you call the built-in function next().

Python will also let you create an iterator implicitly. E.g. in the context of a FOR loop. In this case, the iterable passed to the FOR loop, is automatically turned into an iterator and returned to the body of the loop.
'''

even_list = list(range(2,50,2))

print(even_list)

## Explicit iterable --> iterator
it1 = iter(even_list)
print("it1 contains: ", type(it1)) ## it1 contains:  <class 'list_iterator'>

# You can call next() on the iterator to step through the values it contains
print(next(it1),next(it1),next(it1),next(it1),next(it1),next(it1)) 

## Implicit iterable --> iterator
print("Implicit iterator from iterable")
for num in even_list:
    print(num) 

## Iterables can also produce generators
'''
A function with a yield statement retuns a generator object.
Generators are iterators. You can call next() on it explicitly, or use it in a context that calls next() implicitly; for e.g. in a for loop.
'''

print("Setup a function that returns a generator")
def joke():
    jk = "Knock, knock..."
    for i in jk:
        yield i  ## returns a generator

print("Print out the values in the generator")

## NOTE: the parenthesis after the function name, executes the function. This function returns a generator.
for val in joke():
    print(val)

'''
A more compact generator syntax is a one liner of the type:
(<expression> for <var> in <iterable> if <condition>)

to be read as, run the for loop and for each value return the expression [if some condition is true]
'''

gen = (l for l in "this is a test")
vgen = (l for l in "this is a test" if l in ("aeiou"))

### Use generator syntax to populate list - list comprehension
l2 = [l for l in "this is a test" if l in ("aeiou")]
print(f'List l2 contains: {l2}')

## Use tuple() function to do a comprehension for a tuple, because the () is already used for generator comprehension.

t1 = tuple((l for l in "this is a test" if l in ("aeiou")))
print(f'Tuple t1 contains: {t1}')
