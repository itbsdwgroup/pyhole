import db_config
import cx_Oracle

# print(f'The list contains: {db_config.bdwe2prd}')
db_conn = cx_Oracle.connect(user=db_config.bdwe2prd[0], password=db_config.bdwe2prd[1], dsn=db_config.bdwe2prd[2])
q_cur = db_conn.cursor()
try:
    res = q_cur.execute(db_config.q1)
except cx_Oracle.Error as e:
    print("Encountered error!")
    print(e)
else:
    print("Some unknown error occured")
db_conn.close()
print("All done!")