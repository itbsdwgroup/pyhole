import logging
import cx_Oracle as ora
import db_registry as db

log = logging.getLogger(__name__)
fh = logging.FileHandler(filename="/tmp/dbq.log", mode='a')
log.setLevel(logging.INFO)
log.addHandler(fh)
# fh.setFormatter(logging.Formatter(fmt='%(asctime)s %(name)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p'))
fh.setFormatter(logging.Formatter(fmt='%(asctime)s %(name)s %(levelname)s %(message)s'))

class Ora:
    # logging.basicConfig(filename=LOGFILE, level=logging.DEBUG, format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')
    conn_cache = {}

    def run_query(self, dbh, query):
        log.info(f"connecting to {db.conn_str[dbh]['user']}@{dbh}") if dbh in db.conn_str else log.warning(f"Connect string {dbh} not found in db registry")
        if dbh in db.conn_str:
            log.info(f"connecting to {db.conn_str[dbh]['user']}@{dbh}")
        else:
            log.warning(f"Connect string {dbh} not found in db registry")
        try:
            conn = ora.connect(user=db.conn_str[dbh]['user'] , password=db.conn_str[dbh]['pass'] , dsn=db.conn_str[dbh]['dsn'])
            log.info(f"Connected to {db.conn_str[dbh]['user']}@{dbh}: {conn.version}")
        except ora.Error as e:
            print("Encountered error!")
            print(e)   
        log.info("Getting query cursor")
        cur = conn.cursor()
        return cur.execute(query)
        
        # Ora.conn_cache[dbh] = ora.connect(user=eval(dbh)['user'], password=eval(dbh)['pass'], dsn=eval(dbh)['dsn'])


