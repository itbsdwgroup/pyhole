'''
This file contains a registry of databases we want to connect to. It is a JDBC like connect string for each 
db alias; similar to a tnsnames.ora file.
'''
bdwe2prd = {
    'user': 'edw',
    'pass': 'p3nc!lBox',
    'dsn': 'localhost:15212/BDWE2PRD.lbl.gov'
}
bdwshare = {
    'user': 'edw',
    'pass': 'f0unta!nP3n',
    'dsn': '127.0.0.1:15212/BDWSHARE.lbl.gov'
}
bdweprd = {
    'user': 'edw',
    'pass': 'drp#5038',
    'dsn': '127.0.0.1:15211/BDWEPRD.lbl.gov'
}

conn_str = {
    'bdwe2prd' : {
        'user': 'edw',
        'pass': 'p3nc!lBox',
        'dsn': 'localhost:15212/BDWE2PRD.lbl.gov'
    },
    'bdwshare' : {
        'user': 'edw',
        'pass': 'f0unta!nP3n',
        'dsn': '127.0.0.1:15212/BDWSHARE.lbl.gov'
    },
    'bdweprd' : {
        'user': 'edw',
        'pass': 'drp#5038',
        'dsn': '127.0.0.1:15211/BDWEPRD.lbl.gov'
    }
}